const std = @import("std");
const view = @import("view.zig");
const utf = std.unicode;
const mem = std.mem;
const log = std.log;
const fs = std.fs;
const io = std.io;
const os = std.os;

const BUFFER_SIZE = 1024 * 4;
const MAX_FRAMES = 20;
const STDIN_HANDLE = os.linux.STDIN_FILENO;
const STDOUT_HANDLE = os.linux.STDOUT_FILENO;

const FileWriter = fs.File.Writer;
const FileReader = fs.File.Reader;
const BuffWriter = io.BufferedWriter(BUFFER_SIZE, FileWriter);

const RawTerm = struct {
    const Self = @This();

    old_state: os.termios,

    pub fn disableRawMode(self: *Self) void {
        os.tcsetattr(STDIN_HANDLE, .FLUSH, self.old_state) catch |err| {
            log.err("Could restore terminal [{}]", .{err});
        };
    }
};

pub fn enableRawMode() !RawTerm {
    const old_term = try os.tcgetattr(STDIN_HANDLE);
    var term = old_term;
    term.lflag &= ~(os.system.ECHO | os.system.ICANON);
    try os.tcsetattr(STDIN_HANDLE, .FLUSH, term);
    return .{ .old_state = old_term };
}

const Input = struct {
    const Self = @This();
    stdin: FileReader,

    // TODO: handle UTF8.
    pub fn nextEvent(self: *Self) !u8 {
        var buf: [20]u8 = undefined;
        const ch = try self.stdin.read(&buf);
        _ = ch;
        return buf[0];
    }
};

pub fn setupEvents() Input {
    const stdin = io.getStdIn().reader();
    return .{ .stdin = stdin };
}

const Screen = struct {
    const Self = @This();

    const Frames = std.BoundedArray(Frame, MAX_FRAMES);

    ally: mem.Allocator,
    frames: Frames,
    writer: BuffWriter,
    cursor: Cursor = .{},

    const Size = struct { rows: u16, cols: u16 };

    const Cursor = struct {
        const Shape = enum { block, blink };
        row: u16 = 1,
        col: u16 = 1,
        shape: Shape = .block,
    };

    pub fn newFrame(self: *Self, rect: Frame.Rect) !*Frame {
        const total = rect.width * rect.height;

        var frame = try self.frames.addOne();
        frame.ally = self.ally;
        frame.rect = rect;
        frame.cells = .{};

        try frame.cells.ensureTotalCapacity(frame.ally, total);
        // for (0..total) |_| try frame.cells.append(frame.ally, .{});

        return frame;
    }

    pub fn render(self: *Self) !void {
        try self.clear();

        const frames = self.frames.constSlice();
        for (frames) |frame| try renderFrame(self, frame);

        try self.reset();
        try self.moveCursor(self.cursor.row, self.cursor.col);
        try self.changeCursor(self.cursor.shape);
        try self.writer.flush();
    }

    pub fn getSize() !Size {
        var ws: os.system.winsize = undefined;

        const err = os.system.ioctl(
            STDIN_HANDLE,
            os.system.T.IOCGWINSZ,
            @intFromPtr(&ws),
        );

        if (os.errno(err) == .SUCCESS) return Size{
            .row = ws.ws_row,
            .col = ws.ws_col,
        };

        return error.terminalSize;
    }

    pub fn setCursor(self: *Self, cursor: Cursor) void {
        self.cursor = cursor;
    }

    pub fn reset(self: *Self) !void {
        const stdout = self.writer.writer();
        try stdout.print("\x1b[0m", .{});
    }

    pub fn setForeground(self: *Self, cl: Frame.Color) !void {
        const stdout = self.writer.writer();
        try stdout.print("\x1b[38;2;{d};{d};{d}m", .{ cl.r, cl.g, cl.b });
    }

    pub fn setBackground(self: *Self, cl: Frame.Color) !void {
        const stdout = self.writer.writer();
        try stdout.print("\x1b[48;2;{d};{d};{d}m", .{ cl.r, cl.g, cl.b });
    }

    pub fn clear(self: *Self) !void {
        const stdout = self.writer.writer();
        try stdout.print("\x1b[2J", .{});
    }

    pub fn moveCursor(self: *Self, row: u16, col: u16) !void {
        const stdout = self.writer.writer();
        try stdout.print("\x1b[{};{}H", .{ row, col });
    }

    pub fn changeCursor(self: *Self, shape: Cursor.Shape) !void {
        const stdout = self.writer.writer();
        switch (shape) {
            .block => try stdout.print("\x1b[\x32 q", .{}),
            .blink => try stdout.print("\x1b[\x35 q", .{}),
        }
    }
};

pub fn getScreen(ally: mem.Allocator) Screen {
    const stdout = std.io.getStdOut().writer();
    const writer = io.bufferedWriter(stdout);
    const frames = Screen.Frames.init(0) catch unreachable;

    return .{
        .ally = ally,
        .writer = writer,
        .frames = frames,
    };
}

const Frame = struct {
    const Cells = std.MultiArrayList(Cell);

    ally: mem.Allocator,
    cells: Cells,
    rect: Rect,

    const Rect = struct {
        x: u16 = 0,
        y: u16 = 0,
        z: u16 = 0,
        width: u16,
        height: u16,
    };

    const Color = struct {
        r: u8,
        g: u8,
        b: u8,
    };

    const Cell = struct {
        rune: u21 = 0,
        fg: Color = .{ .r = 255, .g = 255, .b = 255 },
        bg: Color = .{ .r = 0, .g = 0, .b = 0 },
    };

    pub fn append(self: *Frame, char: u21) !void {
        try self.cells.append(self.ally, .{ .rune = char });
    }

    pub fn deinit(self: *Frame) void {
        self.cells.deinit(self.ally);
    }
};

fn renderFrame(screen: *Screen, frame: Frame) !void {
    const stdout = screen.writer.writer();
    const cells = frame.cells.slice();
    const runes = cells.items(.rune);

    try screen.moveCursor(frame.rect.y, frame.rect.x);

    for (runes) |rune| {
        var bytes: [4]u8 = undefined;
        const len = try utf.utf8Encode(rune, &bytes);
        _ = try stdout.write(bytes[0..len]);
    }
}
