const std = @import("std");
const state = @import("state.zig");
const view = @import("view.zig");
const log = std.log;
const mem = std.mem;
const fs = std.fs;

const String = std.ArrayList(u8);

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const ally = gpa.allocator();

    var raw_term = try view.enableRawMode();
    defer raw_term.disableRawMode();

    var content = try state.newState(ally, "build.zig");
    defer content.text.deinit();

    var events = view.setupEvents();

    var screen = view.getScreen(ally);

    var frame = try screen.newFrame(.{
        .width = 10,
        .height = 10,
    });

    var iter = content.text.iter();
    while (iter.next()) |rune| try frame.append(rune);

    defer frame.deinit();

    var row: u16 = 1;
    var col: u16 = 1;

    while (true) {
        screen.setCursor(.{ .row = row, .col = col });
        try screen.render();

        const char = try events.nextEvent();
        switch (char) {
            'q' => break,
            'l' => col +|= 1,
            'h' => col -|= 1,
            'j' => row +|= 1,
            'k' => row -|= 1,
            else => {},
        }
    }

    std.log.info("Success !!!", .{});
}
