const std = @import("std");
const mem = std.mem;
const utf = std.unicode;

const State = struct {
    text: PieceTable(u21),
};

pub fn newState(ally: mem.Allocator, file_name: ?[]const u8) !State {
    var state: State = .{ .text = PieceTable(u21).init(ally) };

    if (file_name) |path| {
        const file = try std.fs.cwd().openFile(path, .{});
        defer file.close();

        const stat = try file.stat();

        // TODO: read directly into State.
        {
            const bytes = try file.reader().readAllAlloc(ally, stat.size);
            defer ally.free(bytes);

            var runes = std.ArrayList(u21).init(ally);
            defer runes.deinit();

            const view = try utf.Utf8View.init(bytes);
            var iter = view.iterator();

            while (iter.nextCodepoint()) |rune| try runes.append(rune);

            try state.text.setSrc(runes.items);
        }
    }

    return state;
}

// TODO: Cache line break positions in each Piece
pub fn PieceTable(comptime T: type) type {
    return struct {
        const Self = @This();

        const Items = std.ArrayList(T);
        const Pieces = std.ArrayList(Piece);
        const LineBreaks = std.ArrayList(IndexInfo);

        src: Items,
        add: Items,
        pcs: Pieces,
        lns: LineBreaks,
        len: usize = 0,

        const Piece = struct {
            idx: usize,
            len: usize,
            typ: enum { src, add },
        };

        const IndexInfo = struct {
            offset: usize,
            piece: usize,
        };

        pub fn init(ally: mem.Allocator) Self {
            return .{
                .src = Items.init(ally),
                .add = Items.init(ally),
                .pcs = Pieces.init(ally),
                .lns = LineBreaks.init(ally),
            };
        }

        pub fn deinit(self: *Self) void {
            self.src.deinit();
            self.add.deinit();
            self.pcs.deinit();
            self.lns.deinit();
        }

        pub fn setSrc(self: *Self, slice: []const T) !void {
            try self.pcs.append(.{ .idx = 0, .len = slice.len, .typ = .src });
            try self.src.appendSlice(slice);
            self.len = slice.len;

            for (slice, 0..) |c, i| {
                if (c == '\n') {
                    try self.lns.append(.{
                        .piece = 0,
                        .offset = i,
                    });
                }
            }
        }

        pub fn itemAt(self: *Self, index: usize) !T {
            if (index > self.len) return error.IndexOutOfBounds;

            const info = self.getPiece(index);
            const piece = self.pcs.items[info.piece];
            const offset = index - info.offset;

            switch (piece.typ) {
                .add => return self.add.items[piece.idx + offset],
                .src => return self.src.items[piece.idx + offset],
            }

            unreachable;
        }

        pub fn insert(self: *Self, index: usize, item: T) !void {
            if (index > self.len) return error.IndexOutOfBounds;

            const add_len = self.add.items.len;
            const info = self.getPiece(index);
            const is_left_boundary = index == info.offset;
            const is_line_break = item == '\n';

            try self.add.append(item);
            self.len += 1;

            if (is_left_boundary and info.piece != 0) {
                var prev = self.pcs.items[info.piece - 1];
                if (prev.typ == .add and prev.idx + prev.len == add_len) {
                    self.pcs.items[info.piece - 1].len += 1;
                    if (is_line_break) try self.addLineBreak(index, info);
                    return;
                }
            }

            if (is_left_boundary) {
                try self.pcs.insert(info.piece, .{
                    .idx = add_len,
                    .len = 1,
                    .typ = .add,
                });

                if (is_line_break) try self.addLineBreak(index, info);
                return;
            }

            var piece = self.pcs.items[info.piece];

            const split_len = index - info.offset;
            self.pcs.items[info.piece].len = split_len;

            try self.pcs.insert(info.piece + 1, .{
                .idx = piece.idx + split_len,
                .len = piece.len - split_len,
                .typ = piece.typ,
            });

            try self.pcs.insert(info.piece + 1, .{
                .idx = add_len,
                .len = 1,
                .typ = .add,
            });

            if (is_line_break) try self.addLineBreak(index, info);
        }

        pub fn delete(self: *Self, index: usize) !void {
            if (index >= self.len) return error.IndexOutOfBounds;
            defer self.len -= 1;

            const info = self.getPiece(index);
            const piece = self.pcs.items[info.piece];
            const is_left_boundary = index == info.offset;
            const is_right_boudary = index == info.offset + piece.len;
            const is_line_break = '\n' == try self.itemAt(index);

            if (is_line_break) self.delLineBreak(index, info);

            if (piece.len == 1) {
                _ = self.pcs.orderedRemove(info.piece);
                return;
            }

            if (is_left_boundary) {
                self.pcs.items[info.piece].idx += 1;
                self.pcs.items[info.piece].len -= 1;
                return;
            }

            if (is_right_boudary) {
                self.pcs.items[info.piece].len -= 1;
                return;
            }

            const split_len = index - info.offset;
            self.pcs.items[info.piece].len = split_len;

            try self.pcs.insert(info.piece + 1, .{
                .idx = piece.idx + split_len + 1,
                .len = piece.len - (split_len + 1),
                .typ = piece.typ,
            });
        }

        fn getPiece(self: *Self, index: usize) IndexInfo {
            var offset: usize = 0;
            var piece: usize = self.pcs.items.len;

            for (self.pcs.items, 0..) |pc, i| {
                if (index < offset + pc.len) {
                    piece = i;
                    break;
                }

                offset += pc.len;
            }

            return .{ .offset = offset, .piece = piece };
        }

        fn addLineBreak(self: *Self, index: usize, info: IndexInfo) !void {
            const pos = self.getLineBreak(index, info);

            if (pos >= self.lns.items.len) {
                try self.lns.append(.{
                    .piece = info.piece,
                    .offset = index - info.offset,
                });
            } else {
                try self.lns.insert(pos + 1, .{
                    .piece = info.piece,
                    .offset = index - info.offset,
                });
            }
        }

        fn delLineBreak(self: *Self, index: usize, info: IndexInfo) void {
            const pos = self.getLineBreak(index, info);
            _ = self.lns.orderedRemove(pos);
        }

        fn getLineBreak(self: *Self, index: usize, info: IndexInfo) usize {
            var low: usize = 0;
            var high: usize = self.lns.items.len -| 1;
            const offset = index - info.offset;

            while (high > low) {
                const mid = (low + high) / 2;
                const line = self.lns.items[mid];

                if (line.piece > info.piece) {
                    high = mid -| 1;
                    continue;
                }

                if (line.piece < info.piece) {
                    low = mid + 1;
                    continue;
                }

                if (line.offset > offset) {
                    high = mid -| 1;
                    continue;
                }

                if (line.offset < offset) {
                    low = mid + 1;
                    continue;
                }

                return mid;
            }

            return (high + low) / 2;
        }

        pub fn iter(self: *Self) Iterator {
            return .{ .table = self };
        }

        const Iterator = struct {
            const Iter = @This();

            table: *const Self,
            piece: usize = 0,
            index: usize = 0,
            slice: ?[]const T = null,

            pub fn next(self: *Iter) ?T {
                if (self.slice) |slice| {
                    if (self.index < slice.len) {
                        defer self.index += 1;
                        return slice[self.index];
                    }
                }

                if (self.piece >= self.table.pcs.items.len) return null;
                const piece = self.table.pcs.items[self.piece];

                self.index = 0;
                self.piece += 1;

                self.slice = switch (piece.typ) {
                    .add => self.table.add.items[piece.idx .. piece.idx + piece.len],
                    .src => self.table.src.items[piece.idx .. piece.idx + piece.len],
                };

                return self.next();
            }
        };
    };
}

test "PieceTable logic" {
    const String = PieceTable(u8);

    var table = String.init(std.testing.allocator);
    defer table.deinit();

    try table.setSrc("A large span of text");

    for ("large ") |_| try table.delete(2);
    for ("English ", 10..) |c, i| try table.insert(i, c);

    const pieces = &[_]String.Piece{
        .{ .idx = 0, .len = 2, .typ = .src },
        .{ .idx = 8, .len = 8, .typ = .src },
        .{ .idx = 0, .len = 8, .typ = .add },
        .{ .idx = 16, .len = 4, .typ = .src },
    };

    try std.testing.expectEqualSlices(String.Piece, pieces, table.pcs.items);
    try std.testing.expect('E' == try table.itemAt(10));

    var iter = table.iter();
    for ("A span of English text") |c| try std.testing.expectEqual(c, iter.next().?);
}

test "PieceTable fuzzy" {
    const Rng = std.rand.DefaultPrng;
    var xoshiro256 = Rng.init(69420);
    const rand = xoshiro256.random();

    var table = PieceTable(u8).init(std.testing.allocator);
    defer table.deinit();

    try table.setSrc("Random");

    for (0..1000) |_| {
        const index = rand.uintLessThan(usize, table.len);
        const char = rand.int(u8);
        try table.insert(index, char);
    }

    var iter = table.iter();
    var i: usize = 0;

    while (iter.next()) |c| : (i += 1) {
        try std.testing.expectEqual(c, try table.itemAt(i));
    }

    while (table.len > 0) {
        const index = rand.uintLessThan(usize, table.len);
        try table.delete(index);
    }
}
